import org.junit.Assert.*;
import org.junit.*;

public class Testare {

    private static PolCalc operatii;
    private static Polynomial pol1;
    private static Polynomial pol2;
    private static Polynomial rezultat;

    public Testare() { }

    @BeforeClass
    public static void setUpBeforeClass() {
        operatii=new PolCalc();
    }

    @Before
    public void setUp(){
        pol1=new Polynomial("");
        pol2=new Polynomial("");
    }

    @Test
    public void testAdunare()
    {
        pol1.createPol("x^3-x+2");
        pol2.createPol("x^2+5x-5");
        rezultat=operatii.adunare(pol1,pol2);
        Assert.assertEquals("+x^3+x^2+4.0x-3.0",rezultat.print());
        System.out.println("Testul pentru adunare a avut loc cu succes");
    }

    @Test
    public void testScadere()
    {
        pol1.createPol("x^3-x+2");
        pol2.createPol("x^2+5x-5");
        rezultat=operatii.scadere(pol1,pol2);
        Assert.assertEquals("+x^3-x^2-6.0x+7.0",rezultat.print());
        System.out.println("Testul pentru scadere a avut loc cu succes");
    }

    @Test
    public void testInmultire()
    {
        pol1.createPol("x-1");
        pol2.createPol("x^2+3x+4");
        rezultat=operatii.inmultire(pol1,pol2);
        Assert.assertEquals("+x^3+2.0x^2+x-4.0",rezultat.print());
        System.out.println("Testul pentru inmultire a avut loc cu succes");
    }

    @Test
    public void testImpartire()
    {
        pol1.createPol("x^5-2x^4+3x^3+x^2+2x-1");
        pol2.createPol("x^2-x+1");
        operatii.impartire(pol1,pol2);
        Polynomial cat = operatii.getQ();
        Polynomial rest = operatii.getR();
        Assert.assertEquals("+x^3-x^2+x+3.0", cat.print());
        Assert.assertEquals("+4.0x-4.0", rest.print());
        System.out.println("Testul pentru impartire a avut loc cu succes");
    }

    @Test
    public void testDerivare()
    {
        pol1.createPol("x^7+4x^5+3x^4+5x+9");
        rezultat=operatii.derivare(pol1);
        Assert.assertEquals("+7.0x^6+20.0x^4+12.0x^3+5.0", rezultat.print());
        System.out.println("Testul pentru derivare a avut loc cu succes");
    }

    @Test
    public void testIntegrare()
    {
        pol1.createPol("7x^6+5x^4+8x^3+2x+4");
        rezultat=operatii.integrare(pol1);
        Assert.assertEquals("+x^7+x^5+2.0x^4+x^2+4.0x", rezultat.print());
        System.out.println("Testul pentru integrare a avut loc cu succes");
    }


}
