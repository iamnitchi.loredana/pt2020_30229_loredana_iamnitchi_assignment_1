
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MainClass extends JFrame {

   // private static final long serialVersionUID = 1L;
    private JPanel pane = new JPanel(new GridBagLayout());
    private GridBagConstraints c = new GridBagConstraints();
   // private JButton button = new JButton("OK");
    private JButton adunare = new JButton("  aduna  ");
    private JButton scadere = new JButton("    scade   ");
    private JButton inmultire = new JButton("inmulteste");
    private JButton impartire = new JButton(" imparte ");
    private JButton derivare = new JButton(" deriveaza");
    private JButton integrare= new JButton("integreaza");
    private JTextField text = new JTextField(30);
    private JTextField text1 = new JTextField(30);
    private JTextField textA = new JTextField(30);
    private JTextField textB = new JTextField(30);
    private JLabel labelA = new JLabel("A: ");
    private JLabel labelB = new JLabel("B: ");
    private JLabel labelQ = new JLabel("Q: ");
    private JLabel labelR = new JLabel("R: ");

   // private JLabel label = new JLabel("result ");
    private PolCalc logic = new PolCalc();

    public MainClass(String name) {
        super(name);
        /*
        c.gridx = 1;
        c.gridy = 6;
        pane.add(button, c);
        button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0)
            {
                String s = textA.getText();
                Polynomial pol= new Polynomial(s);
                pol.createPol(s);
                Polynomial pol2 = pol.inverseaza();
                text.setText(pol2.print());
            }
        });
     */
        c.gridx = 0;
        c.gridy = 0;
        pane.add(labelA, c);

        c.gridx = 0;
        c.gridy = 1;
        pane.add(labelB, c);

        c.gridx = 1;
        c.gridy = 0;
        pane.add(textA,c);

        c.gridx = 1;
        c.gridy = 1;
        pane.add(textB,c);

        c.gridx = 0;
        c.gridy = 2;
        pane.add(adunare, c);
        adunare.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0)
            {
                String s = textA.getText();
                Polynomial pol= new Polynomial(s);
                pol.createPol(s);

                String s1 = textB.getText();
                Polynomial pol1= new Polynomial(s1);
                pol1.createPol(s1);

                Polynomial res= logic.adunare(pol, pol1);;
                text.setText(res.print());
            }
        });

        c.gridx = 1;
        c.gridy = 2;
        pane.add(scadere, c);
        scadere.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0)
            {
                String s = textA.getText();
                Polynomial pol= new Polynomial(s);
                pol.createPol(s);

                String s1 = textB.getText();
                Polynomial pol1= new Polynomial(s1);
                pol1.createPol(s1);

                Polynomial res= logic.scadere(pol, pol1);;
                text.setText(res.print());
            }
        });

        c.gridx = 2;
        c.gridy = 2;
        pane.add(inmultire, c);
        inmultire.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0)
            {
                String s = textA.getText();
                Polynomial pol= new Polynomial(s);
                pol.createPol(s);

                String s1 = textB.getText();
                Polynomial pol1= new Polynomial(s1);
                pol1.createPol(s1);
                if(s.length()==0 || s1.length()==0)
                  text.setText("0");
                else
                    {
                    Polynomial res=logic.inmultire(pol, pol1);
                    text.setText(res.print());
                   }
            }
        });

        c.gridx = 0;
        c.gridy = 3;
        pane.add(impartire, c);
        impartire.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0)
            {
                String s = textA.getText();
                Polynomial pol= new Polynomial(s);
                pol.createPol(s);

                String s1 = textB.getText();
                Polynomial pol1= new Polynomial(s1);
                pol1.createPol(s1);

                if(s.length()==0)
                    text.setText("0");
                else
                    if(s1.length()==0)
                        text.setText("Impartirea nu se poate efectua");
                    else
                    {
                        PolCalc pol2=new PolCalc();
                        pol2.impartire(pol, pol1);
                        Polynomial Q=pol2.getQ();
                        Polynomial R=pol2.getR();
                        text.setText(Q.print());
                        text1.setText(R.print());
                    }
            }
        });

        c.gridx = 1;
        c.gridy = 3;
        pane.add(derivare, c);
        derivare.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0)
            {
                String s = textA.getText();
                if(s.length()==0)
                    text.setText("Introduceti textul in primul text field!");
                else {
                    Polynomial pol = new Polynomial(s);
                    pol.createPol(s);
                    Polynomial res = logic.derivare(pol);;
                    text.setText(res.print());
                }
            }
        });

        c.gridx = 2;
        c.gridy = 3;
        pane.add(integrare, c);
        integrare.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0)
            {
                String s = textA.getText();
                if(s.length()==0)
                    text.setText("Introduceti textul in primul text field!");
                else
                {
                    Polynomial pol= new Polynomial(s);
                    pol.createPol(s);
                    Polynomial res=logic.integrare(pol);
                    text.setText(res.print());
                }
            }
        });

        c.gridx = 1;
        c.gridy = 4;
        pane.add(text, c);

        c.gridx = 1;
        c.gridy = 5;
        pane.add(text1, c);

        c.gridx = 0;
        c.gridy = 4;
        pane.add(labelQ,c);

        c.gridx = 0;
        c.gridy = 5;
        pane.add(labelR,c);

        this.add(pane);
    }

    public static void main(String args[]) {
        JFrame frame = new MainClass("Calculator de polinoame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}