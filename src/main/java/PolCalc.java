import static java.util.Collections.sort;

public class PolCalc {
    private Polynomial aux1;
    private Polynomial Q = new Polynomial("");
    private Polynomial R = new Polynomial("");
    private Monomial m1 = new Monomial();
    private Monomial m2 = new Monomial();
    private Monomial m3, m4;

    public PolCalc() { }

    public Polynomial adunare(Polynomial pol1, Polynomial pol2)
    {   Polynomial res=new Polynomial("");
        int i = 0, j = 0;
        while (i < pol1.getPol().size() && j < pol2.getPol().size()) {
            m1 = pol1.getPol().get(i);
            m2 = pol2.getPol().get(j);
            if (m1.getExp() == m2.getExp()) {
                m3 = new Monomial(m1.getCoef() + m2.getCoef(), m1.getExp());
                i++; j++;
            } else if (m1.getExp() > m2.getExp()) {
                m3 = new Monomial(m1.getCoef(), m1.getExp());
                i++;
            } else {
                m3 = new Monomial(m2.getCoef(), m2.getExp());
                j++; }
            res.addMonom(m3);
        }
        while (i < pol1.getPol().size()) {
            m1 = pol1.getPol().get(i);
            m3 = new Monomial(m1.getCoef(), m1.getExp());
            i++;
            res.addMonom(m3);
        }
        while (j < pol2.getPol().size()) {
            m2 = pol2.getPol().get(j);
            m3 = new Monomial(m2.getCoef(), m2.getExp());
            j++;
            res.addMonom(m3); }
        return reduce(res);
    }

    public Polynomial scadere(Polynomial pol1, Polynomial pol2)
    {
        Polynomial pol3=pol2.inverseaza();
        Polynomial res=adunare(pol1,pol3);
        return reduce(res);
    }

    public Polynomial inmultire(Polynomial pol1, Polynomial pol2) {
        Polynomial res=new Polynomial("");
        int i=0,j=0;
        int n1=pol1.getPol().size();
        int n2=pol2.getPol().size();
        while(i<n1) {
            m1 = pol1.getPol().get(i);
            j=0;
            while(j<n2) {
                m2 = pol2.getPol().get(j);
                m3=new Monomial(m1.getCoef()*m2.getCoef(), m1.getExp()+m2.getExp());
                j++;
                res.addMonom(m3);
            }
            i++;
        }
        return reduce(res);
    }

    public Polynomial derivare(Polynomial pol) {
        Polynomial res=new Polynomial("");
        int i=0;
        int n=pol.getPol().size();
        while(i<n) {
            m1 = pol.getPol().get(i);
            m3=new Monomial(m1.getCoef()*m1.getExp(), m1.getExp()-1);
            i++;
            res.addMonom(m3);
        }
        return reduce(res);
    }

    public Polynomial integrare(Polynomial pol) {
        Polynomial res=new Polynomial("");
        int i=0;
        int n=pol.getPol().size();
        while(i<n) {
            m1 = pol.getPol().get(i);
            m3=new Monomial(m1.getCoef()/(m1.getExp()+1), m1.getExp()+1);
            i++;
            res.addMonom(m3);
        }
        return reduce(res);
    }

    public void sortare(Polynomial pol)
    {
        sort(pol.getPol());
    }

    public Polynomial reduce(Polynomial pol)
    {
        Polynomial reducedPol=new Polynomial("");
        sortare(pol);
        int i=0,j=0;
        double sum=0;
        int n=pol.getPol().size();
        while(i<n) {
            j=i+1;
            m1 = pol.getPol().get(i);
            sum=m1.getCoef();
            while(j<n) {
                m2=pol.getPol().get(j);
                if(m1.getExp()==m2.getExp()) {
                    sum+= m2.getCoef();
                    j++; }
                else
                    break;
            }
            m3 = new Monomial(sum, m1.getExp());
            reducedPol.addMonom(m3);
            i=j;
        }
        return reducedPol;
    }

    private void deleteEl(Polynomial pol)
    {
        int i=0;
        while(i<pol.getPol().size())
        {
            if(pol.getPol().get(i).getCoef()==0)
            {
                pol.getPol().remove(pol.getPol().get(i));
                i++;
            }
           else
               i++;
        }
    }

    public void impartire(Polynomial a, Polynomial c)
    {
        Polynomial P=reduce(a);
        Polynomial B=reduce(c);
        R=P;
        while(R.getGrad()>B.getGrad() || R.getGrad()==B.getGrad() )
       {
            m4 = new Monomial(R.getPol().get(0).getCoef() / B.getPol().get(0).getCoef(), R.getPol().get(0).getExp() - B.getPol().get(0).getExp());
            Polynomial N = new Polynomial("");
            P=R;
            N.addMonom(m4);
            Q.addMonom(m4);
            aux1 = inmultire(N, B);
            R = scadere(P, aux1);
            deleteEl(R);
      }
    }

    public Polynomial getQ() { return reduce(Q); }

    public Polynomial getR() { return reduce(R); }
}
