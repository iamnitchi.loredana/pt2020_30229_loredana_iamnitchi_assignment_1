public class Monomial implements Comparable {
    private double coef;
    private int exp;

    public Monomial ()
    {
        this.coef=0;
        this.exp=0;
    }
    public Monomial (double coef, int exp)
    {
        this.coef=coef;
        this.exp=exp;
    }

    public double getCoef() {
        return coef;
    }

    public int getExp() {
        return exp;
    }

    public int compareTo(Object o) {
        if(((Monomial)o).getExp() > this.getExp())
            return 1;
        else
            if(((Monomial)o).getExp() < this.getExp())
                return -1;
            else
                return 0;
    }

    public String toString() {
      if(coef==0)
         return ""; // 0
      else
        if(exp==0)
            return coef+""; //termen liber pozitiv sau negativ
        else
          if(coef==1 && exp==1)
                return "x"; // x
          else
            if(coef==-1 && exp==1)
                return "-x"; // -x
            else
                if(coef!=1 && coef!=-1 && exp==1)
                    return coef + "x"; //2x,3x si -2x, -3x
                else
                    if(coef==1 && exp!=1) //x^2
                        return "x^"+exp;
                     else
                         if(coef==-1  && exp!=1) //-x^2
                             return "-x^"+exp;
                         else
                             return coef+ "x^"+exp; //2x^4 si -2x^4
    }
}
