import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.sort;

public class Polynomial{
    private List<Monomial> pol;
    private String t;
    private String [] r;

    public Polynomial(String t)
    {
        this.t=t;
        this.pol=new ArrayList<Monomial>();
    }

    public void createPol(String t1) {
        t1=t1.replace("-","+-");
        r=t1.split("\\+");
        for (String s:r) {
            if (s.length() != 0) {
                if (!s.startsWith("-"))  //cazuri pozitive
                    testcasePoz(s);
                else //cazuri negative
                    testcaseNeg(s);
            }
        }
        sort(pol);
    }

    private void testcasePoz (String s)
    {
        if (!s.contains("x")) {// cazul 1: termen liber pozitiv
            Monomial m = new Monomial(Double.parseDouble(s), 0);
            pol.add(m);
        } else {
            if (s.equals("x")) { //cazul 2: x --> coeficientul este 1
                Monomial m = new Monomial(1, 1);
                pol.add(m);
            } else {
                if (s.endsWith("x")) { //cazul 3: 2x, 3x etc -->se termina cu x. iau substring-ul care nu contine x, adica pana la lungime-1
                    Monomial m = new Monomial(Double.parseDouble(s.substring(0, s.length() - 1)), 1);
                    pol.add(m);
                } else {
                    if (s.startsWith("x^")) { //cazul 4: x la o putere-->incepe cu x. coeficientul incepe de pe poz 2
                        Monomial m = new Monomial(1, Integer.parseInt(s.substring(2)));
                        pol.add(m);
                    } else { //cazul 5: coef* x la o putere
                        String str1=s.substring(0, s.indexOf("x"));
                        String str2=s.substring(s.indexOf("x") + 2);
                        Monomial m = new Monomial(Double.parseDouble(str1),  Integer.parseInt(str2));
                        pol.add(m);
                    }
                }
            }
        }
    }


    private void testcaseNeg(String s)
    {
        if (!s.contains("x")) {// cazul 1.2: termen liber negativ
            Monomial m = new Monomial(Double.parseDouble(s), 0);
            pol.add(m);
        } else {
            if (s.equals("-x")) { //cazul 2.2: -x --> coeficientul este -1
                Monomial m = new Monomial(-1, 1);
                pol.add(m);
            } else {
                if (s.endsWith("x")) { //cazul 3.2: -2x,-3x--> se termina cu x. iau substring-ul care nu contine x, adica pana la lungime-1
                    Monomial m = new Monomial(Double.parseDouble(s.substring(0, s.length() - 1)), 1);
                    pol.add(m);
                } else {
                    if (s.startsWith("-x^")) { //cazul 4.2: -x la o putere --> incepe cu x. coeficientul incepe de pe poz 2
                        Monomial m = new Monomial(-1, Integer.parseInt(s.substring(3)));
                        pol.add(m);
                    } else {  //cazul 5.2: -coef* x la o putere
                        String str1 = s.substring(0, s.indexOf("x")); //coeficientul de la poz 1 pana la x
                        String str2 = s.substring(s.indexOf("x")+ 2); //exponentul de la ^ pana la sfarsit
                        Monomial m = new Monomial(Double.parseDouble(str1), Integer.parseInt(str2));
                        pol.add(m);
                    }
                }
            }
        }
    }

    public int getGrad()
    {
        sort(pol);
        return pol.get(0).getExp();
    }

    public void addMonom(Monomial m) { pol.add(m); }

    public String[] getR() { return r; }

    public List<Monomial> getPol() { return pol; }

    public Polynomial inverseaza()
    {
        Polynomial pol2=new Polynomial("");
        for(Monomial m: pol)
        {
            pol2.addMonom( new Monomial(-m.getCoef(), m.getExp()));
        }
        return pol2;
    }

    public String getT() { return t; }

    public String print()
    {
        String finals="";
        for(Monomial m: pol)
            if(m.toString().startsWith("-"))
             finals+=m.toString();
            else
                if(m.getCoef()==0)
                    finals+="";
                else
                    finals+="+" + m.toString();
        return finals;
    }

}
